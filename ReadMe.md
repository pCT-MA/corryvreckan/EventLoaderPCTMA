# EventLoaderPCTMA
**Maintainer**:  
**Module Type**: *GLOBAL*  
**Status**: Functional

### Description
This repository tracks a corryvreckan (version 1.0.3) event loader module and should be placed like `corryvreckan-root-directory/src/modules/EventLoaderPCTMA`. It loads raw or trp data from the proton CT at MedAustron tracker and stores pixel hits and clusters on the clipboard.

### Dependencies

pugixml and svd_slow_ctrl are added as submodules. To initially clone them with
the project. From the root directory of corryvreckan: 

```bash
git clone --recurse-submodules https://gitlab.com/pCT-MA/corryvreckan/EventLoaderPCTMA src/modules/EventLoaderPCTMA
```

Alternatively, submodules may be loaded after cloning, from the EventLoaderPCTMA
directory: 

```bash
git submodule sync
git submodule update --init
```

Change to the corryvreckan build directory and set "BUILD_EventLoaderPCTMA" during the cmake configuration,

```bash
cd build
cmake .. "-DBUILD_EventLoaderPCTMA=ON"
```

### Parameters

- `data_directory`: directory that contains the data (_.dat_) files. 
- `config_directory`: directory that contains the xml copies from the measurement config files. It is likely identical to `data_directory`, but the parameter allows to store config and data in separate directories
- `config_files`: list of config-0, -1.xml file names that make up a run

The parameters `first_pixel_numbers` and `frontend_ids` are a workaround for sensors that connected strips to a lowest pixel number $\neq$ 0, such as the diamond detector.
The readout chip on this sensor is bonded to the fourth slot of the hybrid connector, and so the strip numbers begin at $3\times 128 = 384$:

![diamond-sensor.jpg](diamond-sensor.jpg)

Both parameters are lists that, together, map $\mathrm{ID}\mapsto\mathrm{first\ pixel}$. They must have equal size and ordering

- `first_pixel_numbers`: list of first pixel numbers on each plane and sensor side
- `frontend_ids`: list of front end ids (_layer.ladder.sensor.side_) that describe sensors and sides

### Usage

```toml
[Corryvreckan]
detectors_file = "2020-05-10.cfg"
detectors_file_updated = "2020-05-10_prealigned.cfg"
histogram_file = "prealignment.root"

[EventLoaderPCTMA]
data_directory = "/home/testbeam/bt_medaustron_diamond_20200510/spy_data_local"
config_directory = "/home/testbeam/bt_medaustron_diamond_20200510/spy_data_local"
config_files = "pct_dia_01_silimond_beam_LocalHardware_01-000002.xml"

frontend_ids = "4.6.1.1"
first_pixel_numbers = 384

[TrackingSpatial]
…
```
