#include <TH2D.h>
#include <string>

#include "EventLoaderPCTMA.h"

using namespace corryvreckan;
using namespace SVD::QM;

SVD::QM::FrontEndID_t ParseFrontendId(std::string const& str); // shuts up -Wmissing-declarations
SVD::QM::FrontEndID_t ParseFrontendId(std::string const& str) {
    std::string split_value;
    std::stringstream separate_values(str);

    std::array<uint8_t, 4> layerLadderSensorSide;
    std::size_t i = 0;
    while(separate_values.good()) {
        std::getline(separate_values, split_value, '.');
        layerLadderSensorSide[i++] = static_cast<uint8_t>(std::stoul(split_value));
    }
    return {
        layerLadderSensorSide.at(0), layerLadderSensorSide.at(1), layerLadderSensorSide.at(2), layerLadderSensorSide.at(3)};
}

EventLoaderPCTMA::EventLoaderPCTMA(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
    : Module(config, detectors), mConfigDirectory(""), mDataDirectory(""), mSeedCut(0.f), mNeighborCut(0.f),
      mClusterCut(0.f), mMinHitLength(0.f), mFrameSignalCut(0.f), mCurrentConfigFile(0), mEventNumberOffset(0),
      mLastEventNumber(0), mLastTriggerLocal(0), mTriggerOverflows(0), mTriggerOverflowSize(0), mFirstEvent(true),
      mFileReader(nullptr), mDetectors(detectors) {

    mConfigDirectory = get_configuration().getPath("config_directory", true);
    mDataDirectory = get_configuration().getPath("data_directory", true);
    mConfigFiles = get_configuration().getArray<std::string>("config_files");

    mSeedCut = get_configuration().get<float>("seed_cut", 0.f);
    mNeighborCut = get_configuration().get<float>("neighbor_cut", 0.f);
    mClusterCut = get_configuration().get<float>("cluster_cut", 0.f);
    mMinHitLength = get_configuration().get<unsigned>("min_hit_length", 0.f);
    mFrameSignalCut = get_configuration().get<float>("frame_signal_cut", 0.f);

    mFirstPixelNumbers = get_configuration().getArray<unsigned>("first_pixel_numbers", {});
    std::vector<std::string> frontendIds = get_configuration().getArray<std::string>("frontend_ids", {});

    get_configuration().setDefault("trigger_overflow_size", 1u << 15u);
    mTriggerOverflowSize = get_configuration().get<uint32_t>("trigger_overflow_size");

    for(auto id : frontendIds) {
        mFrontEndIds.push_back(ParseFrontendId(id).m_ID);
    }

    if(mFrontEndIds.size() != mFirstPixelNumbers.size()) {
        throw std::invalid_argument("Size mismatch in first_pixel_numbers and frontend_ids parameters");
    }
}

void EventLoaderPCTMA::initialize() {
    for(auto detector : mDetectors) {
        const auto& name = detector->getName();
        mDetectorHitMaps[name] = new TH2D(("hitmap_" + name).c_str(),
                                          ("hitmap_" + name).c_str(),
                                          detector->nPixels().X(),
                                          0,
                                          detector->nPixels().X(),
                                          detector->nPixels().Y(),
                                          0,
                                          detector->nPixels().Y());
    }

    if(!InitialiseCurrentDataFile()) {
        throw ModuleError("Unable to read input file \"" + GetCurrentConfigFile() + "\"");
    }
}

StatusCode EventLoaderPCTMA::run(std::shared_ptr<Clipboard> const& clipboard) {
    bool readSuccess = ReadEventCurrentFile(clipboard);
    if(readSuccess)
        mFirstEvent = false;

    if(mFileReader->GetState() == Reader_t::State_t::Finished && (mCurrentConfigFile + 1) < mConfigFiles.size()) {
        ++mCurrentConfigFile;
        mEventNumberOffset = mLastEventNumber + 1;
        if(!InitialiseCurrentDataFile()) {
            throw ModuleError("Unable to read input file \"" + GetCurrentConfigFile() + "\"");
        }
    }

    return StateToCorry(mFileReader->GetState());
}

void EventLoaderPCTMA::finalize(std::shared_ptr<ReadonlyClipboard> const&) {
    mFileReader->Exit();
}

std::vector<SVD::QM::FrontEndID_t> GetSideIds(std::vector<SVD::QM::FrontEndID_t> const& ids,
                                              std::size_t side); // shuts up -Wmissing-declarations
std::vector<SVD::QM::FrontEndID_t> GetSideIds(std::vector<SVD::QM::FrontEndID_t> const& ids, std::size_t side) {
    std::vector<SVD::QM::FrontEndID_t> result;
    for(const auto& id : ids) {
        if(id.GetSide() == side) {
            result.push_back(id);
        }
    }
    return result;
}

bool IsSameSensorDifferentSide(SVD::QM::FrontEndID_t const& a,
                               SVD::QM::FrontEndID_t const& b); // shuts up -Wmissing-declarations
bool IsSameSensorDifferentSide(SVD::QM::FrontEndID_t const& a, SVD::QM::FrontEndID_t const& b) {
    return a.GetLadder() == b.GetLadder() && a.GetLayer() == b.GetLayer() && a.GetSensor() == b.GetSensor() &&
           a.GetSide() != b.GetSide();
}

std::vector<std::pair<SVD::QM::FrontEndID_t, SVD::QM::FrontEndID_t>>
EventLoaderPCTMA::InterpretAsPixels(std::vector<SVD::QM::FrontEndID_t> const& ids) {
    std::vector<std::pair<SVD::QM::FrontEndID_t, SVD::QM::FrontEndID_t>> result;
    std::vector<SVD::QM::FrontEndID_t> RemainingPIds = GetSideIds(ids, gPSide);
    std::vector<SVD::QM::FrontEndID_t> RemainingNIds = GetSideIds(ids, gNSide);
    bool foundMatch = false;
    std::size_t matchingN = 0;
    for(std::size_t i = 0; i < RemainingPIds.size(); i++) {
        for(std::size_t j = 0; j < RemainingNIds.size(); j++) {
            if(IsSameSensorDifferentSide(RemainingPIds.at(i), RemainingNIds.at(j))) {
                foundMatch = true;
                matchingN = j;
            }
        }
        if(!foundMatch) {
            std::string idAsString;
            std::stringstream stream;
            stream << RemainingNIds.at(i);
            stream >> idAsString;
            throw std::logic_error("Did not find matching p side to n side: \"" + idAsString + "\".");
        }
        result.push_back({RemainingPIds.at(i), RemainingNIds.at(matchingN)});
        RemainingNIds.erase(RemainingNIds.begin() + static_cast<long long>(matchingN));
        // cleanup
        foundMatch = false;
    }
    return result;
}

std::string EventLoaderPCTMA::FormatDetectorName(SVD::QM::FrontEndID_t const& id) {
    return std::string(gDetectorPrefix) + "_" + std::to_string(id.GetLayer()) + "_" + std::to_string(id.GetLadder()) + "_" +
           std::to_string(id.GetSensor());
}

StatusCode EventLoaderPCTMA::StateToCorry(SVD::QM::Reader_t::State_t state) {
    switch(state) {
    case SVD::QM::Reader_t::State_t::Closed:
        return StatusCode::DeadTime;
    case SVD::QM::Reader_t::State_t::Waiting:
        return StatusCode::DeadTime;
    case SVD::QM::Reader_t::State_t::Opened:
        return StatusCode::Success;
    case SVD::QM::Reader_t::State_t::Finished:
        return StatusCode::EndRun;
    case SVD::QM::Reader_t::State_t::Error:
        return StatusCode::Failure;
    default:
        break;
    }
    return StatusCode::Failure;
}

bool EventLoaderPCTMA::InitialiseCurrentDataFile() {
    bool success = false;
    try {
        const auto configFile = GetCurrentConfigFile();
        LOG(INFO) << "Begin reading: " << configFile;
        mConfig = ReadXMLConfig()(configFile);
        mConfig->m_ReaderConfig.m_Path = mDataDirectory;
        if(mSeedCut > 0.f)
            mConfig->m_TriggerRunConfig.m_SeedCut = mSeedCut;
        if(mNeighborCut > 0.f)
            mConfig->m_TriggerRunConfig.m_NeighborCut = mNeighborCut;
        if(mClusterCut > 0.f)
            mConfig->m_TriggerRunConfig.m_ClusterCut = mClusterCut;
        if(mMinHitLength > 0)
            mConfig->m_TriggerRunConfig.m_MinHitLenght = static_cast<uint8_t>(mMinHitLength);
        if(mFrameSignalCut > 0.f)
            mConfig->m_TriggerRunConfig.m_FrameSignalCut = mFrameSignalCut;
        mFileReader.reset(new FileReader(mConfig.get()));

        mLocalStorage.reset(new LocalStorage_t(mConfig->m_FrontEnd.GetComponent<FrontEndID_t>()));

        // Initialisation of components
        RawData_t::Init(mConfig.get(), mLocalStorage->GetComponent<RawData_t>());
        TRPData_t::Init(mConfig.get(), mLocalStorage->GetComponent<TRPData_t>());
        mConfig->m_FrontEnd.Copy<Noise_t>(mLocalStorage->GetComponent<Noise_t>());
        InitHybridCluster_t(mConfig.get(), mLocalStorage->GetComponent<HybridCluster_t>());
        mConfig->m_FrontEnd.Copy<ChannelMask_t>(mLocalStorage->GetComponent<ChannelMask_t>());
        APV25Error_t::Init(mConfig.get(), mLocalStorage->GetComponent<APV25Error_t>());

        // create data processors
        mUnpacker.reset(new UnpackFADC<LocalStorage_t>(mConfig.get()));
        mRaw2TRP.reset(new Raw2TRP(mConfig.get()));
        mApplyPed.reset(new ApplyPedestal(mConfig.get()));
        mApplyCMC.reset(new ApplyCMC(mConfig.get()));

        mEvalSyncError.reset(new EvalSyncError(mConfig.get()));
        mTrp2HybridCluster.reset(new TRP2HybridCluster(mConfig.get()));

        mPixelIds = InterpretAsPixels(mConfig->m_FrontEnd.GetComponent<FrontEndID_t>());
        mEvent.reset(new FADCData_t());
        mFileReader->Open();
        success = true;
        // super awful catch-all :)
    } catch(const std::exception& e) {
        // TODO: convert to corry logging:

        // Logger::Log(e);
        // PrintProgressFinish(0);
        // std::exit(EXIT_FAILURE);
    }
    return success;
}

bool EventLoaderPCTMA::ReadEventCurrentFile(std::shared_ptr<Clipboard> clipboard) {
    try {
        if(mFileReader->ReadEvent(*mEvent.get())) {
            mLastEventNumber = static_cast<uint32_t>(mEvent->m_EventNumber);

            (*mUnpacker)(mEvent->m_Data, *mLocalStorage);

            // if raw mode data transform to trp data compatiable with ZS
            if(mConfig->m_FADCConfig.m_DataMode == SVD::Defs::DataMode_t::Raw) {
                (*mRaw2TRP.get())(mLocalStorage->GetComponent<RawData_t>(),
                                  mLocalStorage->GetComponent<TRPData_t>(),
                                  mLocalStorage->GetComponent<APV25Error_t>());
                (*mApplyPed.get())(mLocalStorage->GetComponent<TRPData_t>(), mConfig->m_FrontEnd.GetComponent<Pedestal_t>());
                (*mApplyCMC.get())(mLocalStorage->GetComponent<TRPData_t>(), mLocalStorage->GetComponent<ChannelMask_t>());
            }

            (*mEvalSyncError)(mLocalStorage->GetComponent<TRPData_t>(), mLocalStorage->GetComponent<APV25Error_t>());
            (*mTrp2HybridCluster)(static_cast<long>(mLastEventNumber),
                                  mLocalStorage->GetComponent<Noise_t>(),
                                  mLocalStorage->GetComponent<ChannelMask_t>(),
                                  mLocalStorage->GetComponent<TRPData_t>(),
                                  mLocalStorage->GetComponent<HybridCluster_t>());
            const auto trigger = mUnpacker->GetMainHeader().m_EventNr;
            mTriggerOverflows = !mFirstEvent && trigger <= mLastTriggerLocal ? mTriggerOverflows + 1 : mTriggerOverflows;
            mLastTriggerLocal = trigger;
            auto globalTrigger = mLastTriggerLocal + mTriggerOverflows * mTriggerOverflowSize;

            // put a new event on the clipboard
            // Since time information is not available, fake it with the event number
            auto event = std::make_shared<Event>(static_cast<double>(mEventNumberOffset + mLastEventNumber) - 0.5,
                                                 static_cast<double>(mEventNumberOffset + mLastEventNumber) + 0.5);
            event->addTrigger(globalTrigger, static_cast<double>(mEventNumberOffset + mLastEventNumber));
            clipboard->putEvent(event);

            for(const auto& ids : mPixelIds) {
                const auto detectorName = FormatDetectorName(ids.first);
                Detector* detector = nullptr;
                for(const auto& det : mDetectors) {
                    if(detectorName == det->getName()) {
                        detector = det.get();
                        break;
                    }
                }
                if(detector == nullptr) {
                    continue;
                }

                ClusterVector result;
                PixelVector fakePixels;
                const auto& pHits = mLocalStorage->template GetValue<SVD::QM::HybridCluster_t>(ids.first);
                const auto& nHits = mLocalStorage->template GetValue<SVD::QM::HybridCluster_t>(ids.second);
                if(pHits.size() == 1 && nHits.size() == 1) {
                    const auto row_first = GetFirstStrip(ids.first.m_ID);
                    const auto column_first = GetFirstStrip(ids.second.m_ID);
                    const auto nHit = nHits.front();
                    const auto pHit = pHits.front();
                    const auto column = nHit.m_COG - static_cast<float>(column_first);
                    const auto row = pHit.m_COG - static_cast<float>(row_first);

                    const auto raw = std::accumulate(std::begin(pHit.m_Signals), std::end(pHit.m_Signals), 0) +
                                     std::accumulate(std::begin(nHit.m_Signals), std::end(nHit.m_Signals), 0);
                    const auto fakePixelSignal = raw / static_cast<double>(nHit.m_Width * pHit.m_Width);

                    auto cluster = std::make_shared<Cluster>();
                    for(std::size_t iC = 0; iC < nHit.m_Width; iC++) {
                        {
                            for(std::size_t iR = 0; iR < pHit.m_Width; iR++) {
                                const auto fakeColumn = static_cast<int>(nHit.m_First + iC - column_first);
                                const auto fakeRow = static_cast<int>(pHit.m_First + iR - row_first);
                                auto fakeHit = std::make_shared<Pixel>(detectorName,
                                                                       fakeColumn,
                                                                       fakeRow,
                                                                       static_cast<int>(fakePixelSignal),
                                                                       fakePixelSignal,
                                                                       0);
                                fakePixels.push_back(fakeHit);
                                cluster->addPixel(fakeHit.get());
                            }
                        }
                    }

                    auto positionLocal = detector->getLocalPosition(column, row);
                    auto positionGlobal = detector->localToGlobal(positionLocal);
                    cluster->setRow(row);
                    cluster->setColumn(column);
                    cluster->setCharge(raw);
                    cluster->setError(detector->getSpatialResolution());

                    cluster->setDetectorID(detectorName);
                    cluster->setClusterCentre(positionGlobal);
                    cluster->setClusterCentreLocal(positionLocal);

                    result.push_back(cluster);
                    mDetectorHitMaps[detectorName]->Fill(static_cast<double>(column), static_cast<double>(row));
                }
                clipboard->putData(fakePixels, detectorName);
                clipboard->putData(result, detectorName);
            }

            // Cleanup
            for(auto& hybrid : mLocalStorage->GetComponent<HybridCluster_t>()) {
                hybrid.clear();
            }
        }
        // super awful catch-all :)
    } catch(const std::exception& e) {
        // TODO: convert to corry logging:

        // Logger::Log(e);
        // PrintProgressFinish(0);
        // std::exit(EXIT_FAILURE);
        return false;
    }
    return true;
}

std::string EventLoaderPCTMA::GetCurrentConfigFile() const {
    return mConfigDirectory + "/" + mConfigFiles.at(mCurrentConfigFile);
}

unsigned EventLoaderPCTMA::GetFirstStrip(uint32_t detector) const {
    assert(mFrontEndIds.size() == mFirstPixelNumbers.size());
    for(std::size_t i = 0; i < mFrontEndIds.size(); i++) {
        if(detector == mFrontEndIds.at(i)) {
            return mFirstPixelNumbers.at(i);
        }
    }
    return 0;
}
