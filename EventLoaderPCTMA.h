#pragma once

#include <../tools/Logger.h>
#include <Analyser_t/ApplyCMC.h>
#include <Analyser_t/ApplyPedestal.h>
#include <Analyser_t/EvalSyncError.h>
#include <Analyser_t/Raw2TRP.h>
#include <Analyser_t/TRP2HybridCluster.h>
#include <Analyser_t/UnpackFADC.h>
#include <CInterface/ReadXMLConfig.h>
#include <DataTypes/FrontEnd_t.h>
#include <Reader_t/FileReader.h>

#include "core/module/Module.hpp"

namespace corryvreckan {
    class EventLoaderPCTMA : public Module {
    public:
        EventLoaderPCTMA(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);
        virtual ~EventLoaderPCTMA() {}

        virtual void initialize() override;

        virtual StatusCode run(std::shared_ptr<Clipboard> const&) override;

        virtual void finalize(std::shared_ptr<ReadonlyClipboard> const&) override;

    private:
        static constexpr std::size_t gPSide = 0;
        static constexpr std::size_t gNSide = 1;
        static constexpr const char* gDetectorPrefix = "dssd";

        using LocalStorage_t = SVD::QM::FrontEnd_t<SVD::QM::RawData_t,
                                                   SVD::QM::TRPData_t,
                                                   SVD::QM::Noise_t,
                                                   SVD::QM::HybridCluster_t,
                                                   SVD::QM::ChannelMask_t,
                                                   SVD::QM::APV25Error_t>;

        std::vector<std::pair<SVD::QM::FrontEndID_t, SVD::QM::FrontEndID_t>> mPixelIds;
        std::string mConfigDirectory;
        std::string mDataDirectory;
        float mSeedCut;
        float mNeighborCut;
        float mClusterCut;
        unsigned mMinHitLength;
        float mFrameSignalCut;
        std::vector<std::string> mConfigFiles;
        std::vector<uint32_t> mFrontEndIds;
        std::vector<unsigned> mFirstPixelNumbers;
        std::size_t mCurrentConfigFile;
        /** counts events beyond configurations (i.e. when using more than one run) */
        uint32_t mEventNumberOffset;
        /** counts events within one configuration (i.e. run) */
        uint32_t mLastEventNumber;
        uint32_t mLastTriggerLocal;
        uint32_t mTriggerOverflows;
        uint32_t mTriggerOverflowSize;
        bool mFirstEvent;
        std::unique_ptr<SVD::QM::Config_t> mConfig;
        std::unique_ptr<SVD::QM::FileReader> mFileReader;
        std::unique_ptr<SVD::QM::UnpackFADC<LocalStorage_t>> mUnpacker;
        std::unique_ptr<SVD::QM::Raw2TRP> mRaw2TRP;
        std::unique_ptr<SVD::QM::ApplyPedestal> mApplyPed;
        std::unique_ptr<SVD::QM::ApplyCMC> mApplyCMC;
        std::unique_ptr<SVD::QM::EvalSyncError> mEvalSyncError;
        std::unique_ptr<SVD::QM::TRP2HybridCluster> mTrp2HybridCluster;

        std::unique_ptr<LocalStorage_t> mLocalStorage;
        std::unique_ptr<SVD::QM::FADCData_t> mEvent;

        std::vector<std::shared_ptr<Detector>> mDetectors;
        std::map<std::string, class TH2D*> mDetectorHitMaps;

        static std::vector<std::pair<SVD::QM::FrontEndID_t, SVD::QM::FrontEndID_t>>
        InterpretAsPixels(std::vector<SVD::QM::FrontEndID_t> const& ids);

        static std::string FormatDetectorName(SVD::QM::FrontEndID_t const& id);

        static StatusCode StateToCorry(SVD::QM::Reader_t::State_t state);

        bool InitialiseCurrentDataFile();
        bool ReadEventCurrentFile(std::shared_ptr<Clipboard> clipboard);
        std::string GetCurrentConfigFile() const;

        unsigned GetFirstStrip(uint32_t detector) const;
    };
} // namespace corryvreckan
