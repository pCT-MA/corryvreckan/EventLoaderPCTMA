CORRYVRECKAN_ENABLE_DEFAULT(OFF)

# Define module and return the generated name as MODULE_NAME
CORRYVRECKAN_GLOBAL_MODULE(MODULE_NAME)

# Add source files to library
CORRYVRECKAN_MODULE_SOURCES(${MODULE_NAME}
    EventLoaderPCTMA.cpp
    # ADD SOURCE FILES HERE...
)

# Provide standard install target
CORRYVRECKAN_MODULE_INSTALL(${MODULE_NAME})

add_subdirectory(pugixml)

set(svdqm_sources svd_slow_ctrl/cpp_ctrls/tools/Error_t.cpp
        svd_slow_ctrl/cpp_ctrls/tools/Defs.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/EvalStripRMS.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/PolyGradientFCN.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/QMConfig_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/CInterface/ReadXMLConfig.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Reader_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Reader_t/FileReader.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/HybridData_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/ResultContainer_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/Histogram_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t.cpp
        svd_slow_ctrl/cpp_ctrls/tools/Logger.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/ADCDelayRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/FADC2Raw.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/Properties_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Core.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/FIRFilterRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/PedestalRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/Raw2TRP.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/NoiseRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/DataProperty.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/CInterface/svdqm_core.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/CalibrationRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/EvalCalibration.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/EvalNoise.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/TriggerRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/FillClusterHistograms.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/TRP2HybridCluster.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/FADC2HybridCluster.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/FADC2TRP.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/CInterface/UpdateInterface.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/CInterface/WriteXMLConfig.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Reader_t/TCPClient.cpp
        svd_slow_ctrl/cpp_ctrls/tools/TCPStream.cpp
        svd_slow_ctrl/cpp_ctrls/tools/TCPConnector.cpp
        svd_slow_ctrl/cpp_ctrls/tools/EpicsLogger.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/SixletCalibrationRun.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/VSepScan.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/IVScan.cpp
        svd_slow_ctrl/cpp_ctrls/tools/TimeStamp.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/EvalHWOccupancy.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/EvalSyncError.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Reader_t/DecodeHeaders.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/DataTypes/StripMask_t.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/UnpackFADC.cpp
        svd_slow_ctrl/cpp_ctrls/svdqm_core/Analyser_t/ApplyCMC.cpp)

set(svdqm_header_directories svd_slow_ctrl/cpp_ctrls/tools
        svd_slow_ctrl/cpp_ctrls/svdqm_core)

add_library(svdqm_core ${svdqm_sources})
set_property(TARGET svdqm_core APPEND_STRING PROPERTY COMPILE_FLAGS "-fPIC")
target_include_directories(svdqm_core PUBLIC ${svdqm_header_directories})
target_link_libraries(svdqm_core pugixml)

# ROOT
# Use: target_link_libraries(${PROJECT_NAME} ${ROOT_LIBRARIES})
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS RIO Minuit2)
include(${ROOT_USE_FILE})

# Threads
# Use: target_link_libraries(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT})
find_package (Threads)

TARGET_LINK_LIBRARIES(${MODULE_NAME} svdqm_core ${CMAKE_THREAD_LIBS_INIT} ${ROOT_LIBRARIES})
